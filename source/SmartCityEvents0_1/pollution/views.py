from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import TemplateView, ListView
from django.core.urlresolvers import reverse_lazy
from .models import Pollution, Notification
from sensors.models import Sensor
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils import timezone
import os, shutil

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class pollutionMainView(TemplateView):
	template_name = 'pollution/pollutionMenu.html'
	model = Pollution
	sucess_url = reverse_lazy('pollution_menu')

	def get(self, request):
		return render_to_response('pollution/pollutionMenu.html', context_instance=RequestContext(request));
		
class pollutionView(TemplateView):
	template_name = 'pollution/mappCR.html'
	model = Pollution
	sucess_url = reverse_lazy('pollution_now')

	def get(self, request, *args, **kwargs):


		type_pollution = request.GET['type_pollution']
		sensors = Sensor.objects.filter(sensorType=type_pollution)
		pollutionList = []
		
		for s in sensors:
			id_current = Pollution.objects.filter(sensor_id=s.id_s, edate__range=[timezone.now() - timezone.timedelta(seconds=300), timezone.now()])
			if(len(id_current)>0):
				value_current = id_current[0]
				for id_c in id_current:
					if id_c.edate > value_current.edate:
						value_current = id_c
				pollutionList.append(value_current)
					
		
		#pollutionList = Pollution.objects.filter(sensor__sensorType=type_pollution, edate__range=[timezone.now() - timezone.timedelta(seconds=300), timezone.now()])

		notificationsDisabled = True
		if 0 != Notification.objects.filter(viewed=False, sensor__sensorType=type_pollution).count():
			notificationsDisabled = False
			
		#find sensors without data
		sensorsError = []
		for s in sensors:
			nofound=True
			for p in pollutionList:
				if p.sensor.id_s == s.id_s:
					nofound = False
			if nofound:
				sensorsError.append(s)

		return render_to_response('pollution/mappCR.html', {'pollutionList': pollutionList, 'sensorsError': sensorsError, 'type_pollution': type_pollution, 'notificationsDisabled': notificationsDisabled}, context_instance=RequestContext(request));


class pollutionNotificationView(TemplateView):
	template_name = 'pollution/notifications.html'
	model =  Notification
	sucess_url = reverse_lazy('Notifications')

	def get(self, request, *args, **kwargs):
		type_pollution = request.GET['type_pollution']
	
		sensorsList = Sensor.objects.filter(sensorType=type_pollution)

		return render_to_response('pollution/notifications.html', {'type_pollution': type_pollution, 'sensorsList': sensorsList}, context_instance=RequestContext(request));

class pollutionNotificationListView(TemplateView):
	template_name = 'pollution/notifications_list.html'
	model =  Notification
	sucess_url = reverse_lazy('NotificationsList')

	def get(self, request, *args, **kwargs):
		type_pollution = request.GET['type_pollution']
		type_alerts = int(request.GET['type_alerts'])
		
		notificationList = Notification.objects.filter(viewed=False, sensor__sensorType=type_pollution)
		if type_alerts == 0:
			notificationList = Notification.objects.filter(viewed=False, sensor__sensorType=type_pollution)
			for n in notificationList:
				n.viewed = True
				n.save()
		elif type_alerts == 1:
			notificationList = Notification.objects.filter(sensor__sensorType=type_pollution, ndate__range=[timezone.now() - timezone.timedelta(days=1), timezone.now()])
		elif type_alerts == 2:
			notificationList = Notification.objects.filter(sensor__sensorType=type_pollution, ndate__range=[timezone.now() - timezone.timedelta(weeks=1), timezone.now()])
		elif type_alerts == 3:
			notificationList = Notification.objects.filter(sensor__sensorType=type_pollution, ndate__range=[timezone.now() - timezone.timedelta(days=30), timezone.now()])
		elif type_alerts == 4:
			notificationList = Notification.objects.filter(sensor__sensorType=type_pollution, ndate__range=[timezone.now() - timezone.timedelta(weeks=52), timezone.now()])

		return render_to_response('pollution/notifications_list.html', {'notificationList': notificationList}, context_instance=RequestContext(request));


class pollutionNotificationListFilterView(TemplateView):
	template_name = 'pollution/notifications_list.html'
	model =  Notification
	sucess_url = reverse_lazy('NotificationsListFilter')

	def post(self, request, *args, **kwargs):

		type_alerts = int(request.POST['type_alerts'])
		
		notificationList = []
		sql = ""

		if type_alerts == 0:
			sql = "viewed=0"
			#notificationList = Notification.objects.filter(viewed=False, sensor__sensorType=request.POST['type_pollution'])
			#for n in notificationList:
			#	n.viewed = True
			#	n.save()
		elif type_alerts == 1:
			sql = "ndate>'" + str(timezone.now() - timezone.timedelta(days=1)) +"'"
		elif type_alerts == 2:
			sql = "ndate>'" + str(timezone.now() - timezone.timedelta(weeks=1)) +"'"
		elif type_alerts == 3:
			sql = "ndate>'" + str(timezone.now() - timezone.timedelta(days=30)) +"'"
		elif type_alerts == 4:
			sql = "ndate>'" + str(timezone.now() - timezone.timedelta(weeks=52)) +"'"
		if str(request.POST['selectSensors']) != "":
			if sql != "":
				sql = sql + " AND "
			sql = sql + "sensor_id=" + str(request.POST['selectSensors'])
		if str(request.POST['alert_value']) != "":
			if sql != "":
				sql = sql + " AND "
			sql = sql + "value>=" + str(request.POST['alert_value'])
		if str(request.POST['alert_value2']) != "":
			if sql != "":
				sql = sql + " AND "
			sql = sql + "value<=" + str(request.POST['alert_value2'])
		if str(request.POST['limit']) != "":
			if sql != "":
				sql = sql + " AND "
			sql = sql + "sensor_id IN (SELECT id_s FROM sensors WHERE alert>=" + str(request.POST['limit']) + ")"
		if str(request.POST['limit2']) != "":
			if sql != "":
				sql = sql + " AND "
			sql = sql + "sensor_id IN (SELECT id_s FROM sensors WHERE alert<=" + str(request.POST['limit2']) + ")"

		sql = 'SELECT * FROM notifications WHERE ' + sql 
		sql = sql +  ' AND sensor_id in (SELECT id_s FROM sensors WHERE sensorType=' + str(request.POST['type_pollution']) + ')'
		notificationList = Notification.objects.raw(sql)

		return render_to_response('pollution/notifications_list.html', {'notificationList': notificationList}, context_instance=RequestContext(request));

class monthView(TemplateView):
	template_name = 'pollution/month.html'
	model = Sensor
	sucess_url = reverse_lazy('month')

	def get(self, request, *args, **kwargs):

		type_pollution = int(request.GET['type_pollution'])
		sensors = Sensor.objects.filter(sensorType=type_pollution)

		return render_to_response('pollution/month.html', {'sensors': sensors}, context_instance=RequestContext(request));

class monthGraphView(TemplateView):
	template_name = 'pollution/graph-month.html'
	model = Pollution
	sucess_url = reverse_lazy('graph_month')

	def get(self, request, *args, **kwargs):

		sensor_id = request.GET['sensor_id']

		load_last_month("graph-month", sensor_id)
		days_month = get_last(30)

		for d in range(len(days_month)):
			days_month[d] = days_month[d].strftime("%d %B %Y")

		return render_to_response('pollution/graph-month.html', {'days_month': days_month}, context_instance=RequestContext(request));

class graphSensorView(TemplateView):
	template_name = 'pollution/datapopup.html'
	model = Pollution
	sucess_url = reverse_lazy('dpopup')
	
	def get(self, request, *args, **kwargs):
		idsensor = request.GET['idsensor']

		shutil.rmtree(os.path.join(BASE_DIR,'static/tsv/sensorgraph.tsv').replace('\\','/'), ignore_errors=True)
		load_file("sensorgraph", "date", "value", idsensor)
		
		now = timezone.now()
		lastweek = timezone.now() - timezone.timedelta(weeks=1)
		
		return render_to_response('pollution/datapopup.html', {'now': now, 'lastweek': lastweek}, context_instance=RequestContext(request));

class pieGraphView(TemplateView):
	template_name = 'pollution/pie_graph.html'
	model = Pollution
	sucess_url = reverse_lazy('pie')

	def get(self, request, *args, **kwargs):

		type_pollution = request.GET['type_pollution']
		sensors_current = Sensor.objects.filter(sensorType=type_pollution)
				
		return render_to_response('pollution/pie_graph.html', {'sensors':sensors_current, 'type_pollution':type_pollution}, context_instance=RequestContext(request));

class pieGraphSensorView(TemplateView):
	template_name = 'pollution/pie_graph_sensor.html'
	model = Pollution
	sucess_url = reverse_lazy('piesensor')

	def get(self, request, *args, **kwargs):

		sensor_id = request.GET['sensor_id']

		sensor_name = 'unknown'
		sensor = Sensor.objects.filter(id_s=sensor_id)
		if sensor != None:
			for s in sensor:
				sensor_name = s.name
		pollutionList = Pollution.objects.filter(sensor_id=sensor_id, edate__range=[timezone.now() - timezone.timedelta(days=365), timezone.now()])
		aqi_eu = [25,50,75,100]
		verylow = 0
		low = 0
		medium = 0
		high = 0
		veryhigh = 0
		for day in range(365):
			max_value=0
			current_day = timezone.now() - timezone.timedelta(days=day)
			pollutionNow = None
			for p in pollutionList:
				if p.edate.date() == current_day.date():
					if(p.value>=max_value):
						max_value=p.value
						pollutionNow=p
			if pollutionNow != None:
				if pollutionNow.value < aqi_eu[0]:
					verylow = verylow+1
				elif pollutionNow.value < aqi_eu[1]:
					low = low+1
				elif pollutionNow.value < aqi_eu[2]:
					medium = medium+1
				elif pollutionNow.value < aqi_eu[3]:
					high = high+1
				elif pollutionNow.value >= aqi_eu[3]:
					veryhigh = veryhigh+1

		return render_to_response('pollution/pie_graph_sensor.html', {'verylow':verylow, 'low':low, 'medium':medium, 'high':high, 'veryhigh':veryhigh, 'sensor_id': sensor_id}, context_instance=RequestContext(request));

class pieGraphMapView(TemplateView):
	template_name = 'pollution/map-pie.html'
	model = Sensor
	sucess_url = reverse_lazy('piemap')

	def get(self, request, *args, **kwargs):
			
		type_pollution = request.GET['type_pollution']
		sensors = Sensor.objects.filter(sensorType=type_pollution);
		return render_to_response('pollution/map-pie.html', {'sensors': sensors}, context_instance=RequestContext(request));

class weekGraphView(TemplateView):
	template_name = 'pollution/graph-week.html'
	model = Sensor
	sucess_url = reverse_lazy('graphweek')

	def get(self, request, *args, **kwargs):
			
		type_pollution = request.GET['type_pollution']
		sensors = Sensor.objects.filter(sensorType=type_pollution)
		load_week_sensors("graph-week", "date", sensors)
		days = get_last(7)
		return render_to_response('pollution/graph-week.html', {'sensors': sensors, 'days': days}, context_instance=RequestContext(request));

class pieDaysListView(TemplateView):
	template_name = 'pollution/pie_daysList.html'
	model = Pollution
	sucess_url = reverse_lazy('piedays')

	def get(self, request, *args, **kwargs):
			
		sensor_id= request.GET['sensor_id']
		level_min = request.GET['level_min']
		level_max = request.GET['level_max']

		pollutionLevelList = []	
		pollutionList = Pollution.objects.filter(sensor_id=sensor_id, edate__range=[timezone.now() - timezone.timedelta(days=365), timezone.now()])
		#pollutionList = Pollution.objects.raw('SELECT max(value), edate, sensor FROM pollution WHERE sensor_id=' + str(sensor_id) + " AND edate>=" + str(timezone.now() - timezone.timedelta(days=365)) + "GROUP BY edate")
		for day in range(365):
			max_value=0
			current_day = timezone.now() - timezone.timedelta(days=day)
			pollutionNow = None
			for p in pollutionList:
				if p.edate.date() == current_day.date():
					if(p.value>=max_value):
						max_value=p.value
						pollutionNow=p
			if pollutionNow != None:
				if (pollutionNow.value <= int(level_max) and pollutionNow.value > int(level_min)):
					pollutionLevelList.append(pollutionNow)

		return render_to_response('pollution/pie_daysList.html', {'pollutionLevelList': pollutionLevelList, 'max': level_max, 'min': level_min}, context_instance=RequestContext(request));


#functions

def load_file(filename, arg1, arg2, arg3=None):

	if not os.path.exists(os.path.join(BASE_DIR,'static/tsv').replace('\\','/')): 
		os.mkdir(os.path.join(BASE_DIR,'static/tsv').replace('\\','/'))

	datafile = open(os.path.join(BASE_DIR,'static/tsv/'+filename+'.tsv').replace('\\','/'), 'w')
	datafile.write(arg1+'\t'+arg2+'\n')

	pollutionWeek = Pollution.objects.filter(sensor_id=arg3, edate__range=[timezone.now() - timezone.timedelta(weeks=1), timezone.now()])
	for pw in pollutionWeek:
		datafile.write(str(pw.edate)+'\t'+str(pw.value)+'\n')

	datafile.close()

	return 0

def load_week_sensors(filename, arg1, sensors):
	if not os.path.exists(os.path.join(BASE_DIR,'static/tsv').replace('\\','/')): 
			os.mkdir(os.path.join(BASE_DIR,'static/tsv').replace('\\','/'))

	for i in range(7):
		shutil.rmtree(os.path.join(BASE_DIR,'static/tsv/graph-week'+str(i)).replace('\\','/'), ignore_errors=True)
		datafile = open(os.path.join(BASE_DIR,'static/tsv/'+filename+str(i)+'.tsv').replace('\\','/'), 'w')
		
		datafile.write(arg1)

		for s in sensors:
			datafile.write('\t'+s.name+' (Sensor '+str(s.id_s)+')')
		datafile.write('\n')
		
		for j in range(24):
			
			#Init to 00:00:00 hours
			current_hour = (timezone.now() - timezone.timedelta(days=i)).replace(microsecond=0, second=0, minute=0, hour=0)
			current_hour = current_hour + timezone.timedelta(hours=j)
			datafile.write(str(current_hour))
			
			for s in sensors:
				add = 0
				pollutionList = Pollution.objects.filter(sensor_id=s.id_s, edate__range=[current_hour - timezone.timedelta(hours=1), current_hour])
				for p in pollutionList:
					add = add + p.value
				if len(pollutionList) != 0:
					add = int(round(add/len(pollutionList)))
				datafile.write('\t'+str(add))
			datafile.write('\n')

		datafile.close()

	return 0

def load_last_month(filename, sensor_id):
	if not os.path.exists(os.path.join(BASE_DIR,'static/tsv').replace('\\','/')): 
		os.mkdir(os.path.join(BASE_DIR,'static/tsv').replace('\\','/'))
	shutil.rmtree(os.path.join(BASE_DIR,'static/tsv/'+filename).replace('\\','/'), ignore_errors=True)
	datafile = open(os.path.join(BASE_DIR,'static/tsv/'+filename+'.tsv').replace('\\','/'), 'w')
	
	days = get_last(30)

	datafile.write('day\thour\tvalue\n')
	for d in range(30):
		#Init to 00:00:00 hours		
		for h in range(24):
			current_hour = (days[d] + timezone.timedelta(hours=h))

			datafile.write(str(d+1)+'\t'+str(h))
		
			add = -1
			pollutionList = Pollution.objects.filter(sensor_id=sensor_id, edate__range=[current_hour, current_hour + timezone.timedelta(hours=1)])
			for p in pollutionList:
				if add < p.value:
					add = int(p.value)
			
			datafile.write('\t'+str(add)+'\n')

	datafile.close()

	return 0

def get_last(num_days=0):
	days = []
	for i in range(num_days):
		days.append((timezone.now() - timezone.timedelta(days=i)).replace(microsecond=0, second=0, minute=0, hour=0))
	return days