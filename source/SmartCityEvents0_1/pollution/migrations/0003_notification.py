# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('pollution', '0002_auto_20160308_1233'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ntype', models.IntegerField(default=0)),
                ('ndate', models.DateTimeField(default=django.utils.timezone.now)),
                ('viewed', models.BooleanField(default=False)),
                ('description', models.TextField(default=b'')),
            ],
            options={
                'db_table': 'notifications',
            },
        ),
    ]
