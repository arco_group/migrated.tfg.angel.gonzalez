# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sensors', '0002_remove_sensor_sensorid'),
        ('pollution', '0004_notification_value'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='sensor',
            field=models.ForeignKey(default=None, to='sensors.Sensor'),
        ),
    ]
