from django.db import models
from django.utils import timezone
from sensors.models import Sensor

class Pollution(models.Model):

	class Meta:
		db_table = "pollution"
		
	value = models.DecimalField(null=False, default=0, max_digits=4, decimal_places=1)
	zone = models.IntegerField(null=False, default=0)
	#alert = models.IntegerField(null=False, default=75)
	other = models.IntegerField(null=True, default=0)
	other2 = models.IntegerField(null=True, default=0)

	#year = models.IntegerField(null=False, default=0)
	#month = models.IntegerField(null=False, default=0)
	#day = models.IntegerField(null=False, default=0)
	#hour = models.IntegerField(null=False, default=0)
	#minute = models.IntegerField(null=False, default=0)
	#second = models.IntegerField(null=False, default=0)
	edate = models.DateTimeField(null=False, default=timezone.now)

	sensor = models.ForeignKey(Sensor, default=None)

	def __unicode__(self):
		return unicode(self.value)

class Notification(models.Model):

	class Meta:
		db_table = "notifications"

	#ntype = models.IntegerField(null=False, default=0)
	sensor = models.ForeignKey(Sensor, default=None)
	value = models.DecimalField(null=False, default=0, max_digits=4, decimal_places=1)
	ndate = models.DateTimeField(null=False, default=timezone.now)
	viewed = models.BooleanField(null=False, default=False)
	description = models.TextField(null=True, default="")
