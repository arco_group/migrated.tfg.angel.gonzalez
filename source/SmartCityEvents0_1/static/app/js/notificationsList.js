$(document).ready(function() {
	$('.alerts_list').html("Loading...");
	$.get("/pollution/notifications/list/", {'type_pollution': $('#type_pollution').val(), 'type_alerts': 0}, function(data){
        $('.alerts_list').html(data);
        menu_height();
    });
});


function changeAlerts() {
	if($('#type_alerts').val() == 0){
		$.get("/pollution/notifications/list/", {'type_pollution': $('#type_pollution').val(), 'type_alerts': 0}, function(data){
	        $('.alerts_list').html(data);
	        menu_height();
	    });
	}else if($('#type_alerts').val() == 1){
		$.get("/pollution/notifications/list/", {'type_pollution': $('#type_pollution').val(), 'type_alerts': 1}, function(data){
	        $('.alerts_list').html(data);
	        menu_height();
	    });
	}else if($('#type_alerts').val() == 2){
		$.get("/pollution/notifications/list/", {'type_pollution': $('#type_pollution').val(), 'type_alerts': 2}, function(data){
	        $('.alerts_list').html(data);
	        menu_height();
	    });
	}else if($('#type_alerts').val() == 3){
		$.get("/pollution/notifications/list/", {'type_pollution': $('#type_pollution').val(), 'type_alerts': 3}, function(data){
	        $('.alerts_list').html(data);
	        menu_height();
	    });
	}else if($('#type_alerts').val() == 4){
		$.get("/pollution/notifications/list/", {'type_pollution': $('#type_pollution').val(), 'type_alerts': 4}, function(data){
	        $('.alerts_list').html(data);
	        menu_height();
	    });
	}
}

$('#type_alerts').on('change', function() {
	$('.alerts_list').html("Loading...");
	menu_height();
	changeAlerts();
});

/*$('#type_alerts').on('click', function() {
	$('.alerts_list').html("Loading...");
	changeAlerts();
});*/

$('#buttonFilter').on('click', function(){
	$('.alerts_list').html("Loading...");
	menu_height();
	$.post("notifications/list/filter/", $('#formFilter').serialize(), function(data){
    	$('.alerts_list').html(data);
    	menu_height();
	});
});