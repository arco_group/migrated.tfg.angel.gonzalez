$(document).ready(function() {

    //To configure a view
    var viewSensors = new ol.View({
        center: ol.proj.transform([-3.9279, 38.9849],'EPSG:4326', 'EPSG:900913'),
        zoom: 14,
        minZoom:14,
        maxZoom:18
    })

    //To create base layer
    var baseLayerSensors = new ol.layer.Tile({
        source: new ol.source.OSM()
    });

    //To create map
    var mapSensors = new ol.Map({
        target: 'map',

        interactions: ol.interaction.defaults(),

        controls: new ol.control.defaults({
            attribution: false,                         //Change for show attribution by OSM
            attributionOptions: ({
                collapsible: false
            })      
        }),

        renderer: 'canvas',
        loadTilesWhileAnimating: true,
        layers: [baseLayerSensors],
        overlays: [],
        view: viewSensors
    });

    var featuresList = loadSensors(mapSensors);
    
    $('#selectSensor').on('change', function() {
        if ($('#selectSensor').val() != ""){                       
            var feature = featureSensor(featuresList, $('#selectSensor').val());
            if(feature){
                if(feature.getGeometry().getRadius() < 800){
                    actionSelect($('#selectSensor'), feature, mapSensors, $('#selectSensor').val(), viewSensors, "select");
                }
            }
        } else {
            load_Empty();
        }

    });

    mapSensors.on('singleclick', function(evt) { 
       var feature = mapSensors.forEachFeatureAtPixel(evt.pixel, function(feature, layer) { return feature; });
        
        if (feature) {   
            if(feature.getGeometry().getRadius() < 800){
                actionClickFeature($('#selectSensor'), feature, mapSensors, evt.pixel, viewSensors);
            }
            else{
                actionClickNoFeature($('#selectSensor'), feature, mapSensors, evt.pixel, viewSensors); 
            }
        } else{       
            actionClickNoFeature($('#selectSensor'), feature, mapSensors, evt.pixel, viewSensors);    
        }
    });
   
});




