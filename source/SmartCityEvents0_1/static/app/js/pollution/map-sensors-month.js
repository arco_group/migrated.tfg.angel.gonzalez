$(document).ready(function() {	
	$.get("/pollution/month/graph/", {'sensor_id': -1}, function(data){
        $('.graph').html("(Select sensor to display monthly heatmap)");
        menu_height();
    });
});

function actionSelect(selector, feature, map, pixel_id, view, mode){
	centerSensor(view, map, pixel_id, mode);
	selectSensor(selector, feature);
	$.get("/pollution/month/graph/", {'sensor_id': parseInt(feature.get('sensor'))}, function(data){
        $('.graph').html(data);
        menu_height(); 
    });
}

function actionClickFeature(selector, feature, map, pixel, view){
    actionSelect(selector, feature, map, pixel,view, "click");    
}


function actionClickNoFeature(selector, feature, map, pixel, view){
	//do nothing 
}

function load_Empty(){
	unselectSensor();
	$.get("/pollution/month/graph/", {'sensor_id': -1}, function(data){
        $('.graph').html("(Select sensor to display monthly heatmap)");
        menu_height(); 
    });
}