$(document).ready(function() {

    //To configure a view
    var viewSensorsOld = new ol.View({
        center: ol.proj.transform([-3.9279, 38.9849],'EPSG:4326', 'EPSG:900913'),
        zoom: 14,
        minZoom:14,
        maxZoom:18
    })
    var viewSensorsNew= new ol.View({
        center: ol.proj.transform([-3.9279, 38.9849],'EPSG:4326', 'EPSG:900913'),
        zoom: 14,
        minZoom:14,
        maxZoom:18
    })

    //To create base layer
    var baseLayerSensors = new ol.layer.Tile({
        source: new ol.source.OSM()
    });

    //To create map
    var mapSensorsNew = new ol.Map({
        target: 'mapnew',

        interactions: ol.interaction.defaults(),

        controls: new ol.control.defaults({
            attribution: false,                         //Change for show attribution by OSM
            attributionOptions: ({
                collapsible: false
            })      
        }),

        renderer: 'canvas',
        loadTilesWhileAnimating: true,
        layers: [baseLayerSensors],
        overlays: [],
        view: viewSensorsNew
    });

     var mapSensorsOld = new ol.Map({
        target: 'mapold',

        interactions: ol.interaction.defaults(),

        controls: new ol.control.defaults({
            attribution: false,                         //Change for show attribution by OSM
            attributionOptions: ({
                collapsible: false
            })      
        }),

        renderer: 'canvas',
        loadTilesWhileAnimating: true,
        layers: [baseLayerSensors],
        overlays: [],
        view: viewSensorsOld
    });

    loadSensors(mapSensorsNew);
    var featuresList = loadSensors(mapSensorsOld);
    
    $('#selectSensor').on('change', function() {
        if($('#selectSensor').val() != ""){            
            var feature = featureSensor(featuresList, $('#selectSensor').val());
            if(feature){
                if(feature.getGeometry().getRadius() < 800){
                    actionSelect($('#selectSensor'), feature, mapSensorsOld, $('#selectSensor').val(), viewSensorsOld, "select");
                }
            }
        }else{
            load_Empty();
        }
    });

    mapSensorsNew.on('singleclick', function(evt) { 
        actionClickFeature(null, null, mapSensorsNew, evt.pixel, null);
    });

    mapSensorsOld.on('singleclick', function(evt) { 
        var feature = mapSensorsOld.forEachFeatureAtPixel(evt.pixel, function(feature, layer) { return feature; });
        if(feature) {
            if(feature.getGeometry().getRadius() < 800){
                actionClickFeatureModify($('#selectSensor'), feature, mapSensorsOld, evt.pixel, viewSensorsOld);
            }
        } else {
            actionClickNoFeatureModify($('#selectSensor'), feature, mapSensorsOld, evt.pixel, viewSensorsOld);
        }
    });

    initialSelected();
    
});




