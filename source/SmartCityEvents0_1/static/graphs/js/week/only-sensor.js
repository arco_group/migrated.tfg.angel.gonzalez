function transitionIn(sensor, day, color){
  
  var svg = d3.select(".graphweek-"+day);
  var margin = {top: 20, right: 80, bottom: 30, left: 50},
  width = svg.attr("width") - margin.left - margin.right,
  height = svg.attr("height") - margin.top - margin.bottom;
  var g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  		
	var x = d3.scaleTime().range([0, width]),
  y = d3.scaleLinear().range([height, 0]),
  z = d3.scaleOrdinal(d3.schemeCategory20);

  var line = d3.line()
    	.curve(d3.curveBasis)
    	.x(function(d) { return x(d.date); })
    	.y(function(d) { return y(d[sensor]); });

	d3.tsv(tsv[day], 
    function(d) {
      d.date = formatDate(d.date);
      d[sensor]= +d[sensor];
      return d;
    }, 
    function(error, data) {
      	if (error) throw error;

      	x.domain(d3.extent(data, function(d) { return d.date; }));
      	
      	y.domain(d3.extent(
      		[-10,
      		d3.max(
      			[110, 
      			d3.max(data, function (d){ return d[sensor]})]
      		)]
 
      	));

      	z.domain(data.map(function(d) { return d[sensor]; }));

      	g.append("g")
        .attr("class", "x axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

      	g.append("g")
          .attr("class", "axis axis--y")
            .call(d3.axisLeft(y))
          .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("fill", "#000")
            .text("Pollution");

  		  g.append("path")
          .datum(data)
          .attr("class", "line")
          .attr("d", line)
          .style("stroke", color);
    });	
}
