from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, patterns, include
from .views import testPollutionView, testAlertPollutionView, testMonthPollutionView, testYearPollutionView, testPollutionSensorsView

urlpatterns = [
	#url(r'^test/$', testView.as_view(), name="test"),
	url(r'^test/pollution/$', testPollutionView.as_view(), name="test_pollution"),
	url(r'^test/alertpollution/$', testAlertPollutionView.as_view(), name="test_alertpollution"),
	url(r'^test/monthpollution/$', testMonthPollutionView.as_view(), name="test_monthpollution"),
	url(r'^test/yearpollution/$', testYearPollutionView.as_view(), name="test_yearpollution"),
	url(r'^test/pollutionSensors/$', testPollutionSensorsView.as_view(), name="test_pollution_sensors"),
    url(r'^pollution/', include('pollution.url')),
    url(r'^management/', include('sensors.url')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

