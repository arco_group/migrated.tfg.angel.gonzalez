# -*- encoding: utf-8 -*-
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, render_to_response
from pollution.models import Pollution, Notification
from sensors.models import Sensor
from django.template import RequestContext
from django.utils import timezone
import random

standarMaxValue = {'Plaza Mayor (centro)': 35, 'Poligono Industrial Larache': 55, 'Parque Industrial Avanzado': 55, 'Hospital General': 20, 'Parque del pilar': 30, 'Campus universitario': 35, 'Parque Reina Sofia': 30, 'Urbanicacion Oeste': 35, 'Calle del Mar Rojo (Glorieta)': 35, 'Parque de Atocha': 30, 'Colegio Publico Santo Tomas': 35, 'Colegio Publico Pio XII': 35}

#normal values
max_normalIndex = 0.8 #index (percent) for max normal value respect to alert value
min_normalIndex = 0.3 #index (percent) for min normal value respect to alert value

def yearsago(years, from_date=None):
    if from_date is None:
        from_date = timezone.now()
    try:
        return from_date.replace(year=from_date.year - years)
    except ValueError:
        # Must be 2/29!
        assert from_date.month == 2 and from_date.day == 29 # can be removed
        return from_date.replace(month=2, day=28,
                                 year=from_date.year-years)

class testView(TemplateView):
	template_name = 'test.html'
	sucess_url = reverse_lazy('test')

	def post(self, request, *args, **kwargs):

		state = False

		data = globals()[request.POST['type']]
		data.value = request.POST['value']
		data.zone = request.POST['zone']
		data.edate = request.POST['date']
		data.save()

		state = True
		dic ={'state': state}

		return render_to_response('test.html', dic, context_instance=RequestContext(request));




class testPollutionSensorsView(TemplateView):

	template_name = 'test_pollution.html'
	model = Sensor
	sucess_url = reverse_lazy('test_pollution_sensors')

	def get(self, request):

		positionLat = [38.9849, 38.97700, 38.98678, 38.97041, 38.98267, 38.99188, 38.99376] #add latitudes to sensor of no2
		positionLat.append(38.98223) #add latitude to sensor of so2
		positionLat.append(38.97393) #add latitude to sensor of co2
		positionLat.append(38.99844) #add latitude to sensor of o3
		positionLat.append(38.97508) #add latitude to sensor of pm2.5
		positionLat.append(38.98504) #add latitude to sensor of pm10
		positionLon = [-3.9279, -3.93226, -3.90674, -3.93149, -3.91711, -3.92031, -3.93620] #add longitudes to sensor of no2
		positionLon.append(-3.94958) #add longitude to sensor of so2
		positionLon.append(-3.92290) #add longitude to sensor of co2
		positionLon.append(-3.92474) #add longitude to sensor of o3
		positionLon.append(-3.94410) #add longitude to sensor of pm2.5
		positionLon.append(-3.94028) #add longitude to sensor of pm10
		alerts =[50, 75, 75, 30, 40, 50, 40] #add alerts to sensor of no2
		alerts.append(50) #add alert to sensor of so2
		alerts.append(50) #add alert to sensor of co2
		alerts.append(40) #add alert to sensor of o3
		alerts.append(50) #add alert to sensor of pm2.5
		alerts.append(50) #add alert to sensor of pm10
		desc = ['Plaza Mayor (centro)', 'Poligono Industrial Larache', 'Parque Industrial Avanzado', 'Hospital General', 'Parque del pilar', 'Campus universitario', 'Parque Reina Sofia'] #add descs to sensor of no2
		desc.append('Urbanicacion Oeste') #add desc to sensor of so2
		desc.append('Calle del Mar Rojo (Glorieta)') #add desc to sensor of co2
		desc.append('Parque de Atocha') #add desc to sensor of o3
		desc.append('Colegio Publico Santo Tomas') #add desc to sensor of pm2.5
		desc.append('Colegio Publico Pio XII') #add desc to sensor of pm10
		type_sensor = [0,0,0,0,0,0,0]
		type_sensor.append(1) #add type to sensor of so2
		type_sensor.append(2) #add type to sensor of co2
		type_sensor.append(3) #add type to sensor of o3
		type_sensor.append(4) #add type to sensor of pm2.5
		type_sensor.append(5) #add type to sensor of pm10
		i = 0
		while i<len(type_sensor):
			sensor = Sensor()
			id_max = 1
			id_current = 1
			sensorsList = Sensor.objects.all()

			if len(sensorsList) == 0:
				sensor.id_s = 1
				id_current = id_current + 1
			else:
				for s in sensorsList:
					if(s.id_s>id_max):
						id_max = s.id_s

				while id_current < id_max:
					if len(Sensor.objects.filter(id_s=id_current)) == 0:
						sensor.id_s = id_current
						id_current = id_max + 1
					id_current = id_current + 1

			if id_current == id_max:
				sensor.id_s = id_max + 1

			sensor.sensorType = type_sensor[i]
			sensor.latitude = positionLat[i]
			sensor.longitude = positionLon[i]
			sensor.description = desc[i]
			sensor.name = desc[i]
			sensor.alert = alerts[i]
			sensor.save()
			i = i+1

		return render(request, self.template_name);



class testPollutionView(TemplateView):

	template_name = 'test_pollution.html'
	model = Pollution
	sucess_url = reverse_lazy('test_pollution')

	def get(self, request):

		sensors = Sensor.objects.all()

		for sen in sensors:

			max_normalValue = int(float(sen.alert) * max_normalIndex)
			p = Pollution()

			if max_normalValue < 15:
				p.value = random.randint(0,max_normalValue+2)
			else:
				p.value = random.randint(max_normalValue-15,max_normalValue+2)
				if p.value < max_normalValue-(15*(max_normalValue-15)/max_normalValue):
					p.value = random.randint(0,max_normalValue-(15*(max_normalValue-15)/max_normalValue))

			if p.value > max_normalValue:  # 1 of 16
					p.value = random.randint(max_normalValue,max_normalValue+16)
					if p.value > max_normalValue+15:  # 1 of 240 (1 of 15)
						p.value = random.randint(max_normalValue+15,max_normalValue+36)
						if p.value > max_normalValue+35: # 1 of 4800 (1 of 20)
							p.value = random.randint(max_normalValue+35,125)
			
			p.zone = 0
			p.other = None
			p.other2 = None
			p.edate = timezone.now()
			p.sensor = sen
			p.save()

			if p.value > sen.alert:
				n = Notification()
				n.sensor = sen
				n.value = p.value
				n.ndate = p.edate
				n.viewed = False
				n.decription = "Value exceed"
				n.save()

		return render(request, self.template_name);





class testAlertPollutionView(TemplateView):

	template_name = 'test_pollution.html'
	model = Pollution
	sucess_url = reverse_lazy('test_alertpollution')

	def get(self, request):

		sensors = Sensor.objects.all()

		for sen in sensors:
			p = Pollution()
			p.value = random.randint(sen.alert, 125)
			p.zone = 0
			p.other = None
			p.other2 = None
			p.edate = timezone.now()
			p.sensor = sen
			p.save()

			if p.value > sen.alert:
				n = Notification()
				n.sensor = sen
				n.value = p.value
				n.ndate = p.edate
				n.viewed = False
				n.decription = "Value exceed"
				n.save()

		return render(request, self.template_name);





class testMonthPollutionView(TemplateView):

	template_name = 'test_pollution.html'
	model = Pollution
	sucess_url = reverse_lazy('test_monthpollution')

	def get(self, request):

		sensors = Sensor.objects.all()

		for sen in sensors:

			now = timezone.now()
			current = timezone.now() - timezone.timedelta(days=30)#.replace(microsecond=0, second=0, minute=0, hour=0)
			while (current < now):
				p = Pollution()
				p.edate = current

				max_normalValue = int(float(sen.alert) * max_normalIndex)

				if max_normalValue < 15:
					p.value = random.randint(0,max_normalValue+2)
				else:
					p.value = random.randint(max_normalValue-15,max_normalValue+2)
					if p.value < max_normalValue-(15*(max_normalValue-15)/max_normalValue):
						p.value = random.randint(0,max_normalValue-(15*(max_normalValue-15)/max_normalValue))

				if p.value > max_normalValue:  # 1 of 16
					p.value = random.randint(max_normalValue,max_normalValue+16)
					if p.value > max_normalValue+15:  # 1 of 240 (1 of 15)
						p.value = random.randint(max_normalValue+15,max_normalValue+36)
						if p.value > max_normalValue+35: # 1 of 4800 (1 of 20)
							p.value = random.randint(max_normalValue+35,125)
	
				p.zone = 0
				p.other = None
				p.other2 = None
				p.sensor = sen
				p.save()

				if p.value > sen.alert:
					n = Notification()
					n.sensor = sen
					n.value = p.value
					n.ndate = p.edate
					n.viewed = False
					n.decription = "Value exceed"
					n.save()

				current = current + timezone.timedelta(hours=1)

		return render(request, self.template_name);





class testYearPollutionView(TemplateView):

	template_name = 'test_pollution.html'
	model = Pollution
	sucess_url = reverse_lazy('test_yearpollution')

	def get(self, request):

		#pollutions = Pollution.objects.all()
		#pollutions.delete()
		#notifications = Notification.objects.all()
		#notifications.delete()

		sensors = Sensor.objects.all()
		
		now = timezone.now()
		date = yearsago(1)
		while date < now-timezone.timedelta(days=31):#144 (12dias)
			for sen in sensors:
				p = Pollution()
				max_normalValue = int(float(sen.alert) * max_normalIndex)
		
				if max_normalValue < 15:
					p.value = random.randint(0,max_normalValue+2)
				else:
					p.value = random.randint(max_normalValue-15,max_normalValue+2)
					if p.value < max_normalValue-(15*(max_normalValue-15)/max_normalValue):
						p.value = random.randint(0,max_normalValue-(15*(max_normalValue-15)/max_normalValue))

				if p.value > max_normalValue:  # 1 of 16
					p.value = random.randint(max_normalValue,max_normalValue+16)
					if p.value > max_normalValue+15:  # 1 of 240 (1 of 15)
						p.value = random.randint(max_normalValue+15,max_normalValue+36)
						if p.value > max_normalValue+35: # 1 of 4800 (1 of 20)
							p.value = random.randint(max_normalValue+35,125)
	
				p.zone = 0
				p.other = None
				p.other2 = None
				p.edate = date
				p.sensor = sen
				p.save()

				if p.value > sen.alert:
					n = Notification()
					n.sensor = sen
					n.value = p.value
					n.ndate = p.edate
					n.viewed = True
					n.decription = "value exceed"
					n.save()
			date = date + timezone.timedelta(days=28)

		date = now-timezone.timedelta(days=31)
		while date < timezone.now()-timezone.timedelta(days=2): #360 (29dias)
			for sen in sensors:
				p = Pollution()
				max_normalValue = int(float(sen.alert) * max_normalIndex)
		
				if max_normalValue < 15:
					p.value = random.randint(0,max_normalValue+2)
				else:
					p.value = random.randint(max_normalValue-15,max_normalValue+1) 
					if p.value < max_normalValue-(15*(max_normalValue-15)/max_normalValue):
						p.value = random.randint(0,max_normalValue-(15*(max_normalValue-15)/max_normalValue))

				if p.value > max_normalValue:  # 1 of 16
					p.value = random.randint(max_normalValue,max_normalValue+16)
					if p.value > max_normalValue+15:  # 1 of 240 (1 of 15)
						p.value = random.randint(max_normalValue+15,max_normalValue+36)
						if p.value > max_normalValue+35: # 1 of 4800 (1 of 20)
							p.value = random.randint(max_normalValue+35,125)

				p.zone = 0
				p.other = None
				p.other2 = None
				p.edate = date
				p.sensor = sen
				p.save()

				if p.value > sen.alert:
					n = Notification()
					n.sensor = sen
					n.value = p.value
					n.ndate = p.edate
					n.viewed = True
					n.decription = "value exceed"
					n.save()
			date = date + timezone.timedelta(days=1)

		date = now -timezone.timedelta(days=1)
		while date < timezone.now()-timezone.timedelta(hours=2): #276 (2dias)
			for sen in sensors:
				p = Pollution()
				max_normalValue = int(float(sen.alert) * max_normalIndex)
		
				if max_normalValue < 15:
					p.value = random.randint(0,max_normalValue+2)
				else:
					p.value = random.randint(max_normalValue-15,max_normalValue+2)
					if p.value < max_normalValue-(15*(max_normalValue-15)/max_normalValue):
						p.value = random.randint(0,max_normalValue-(15*(max_normalValue-15)/max_normalValue))

				if p.value > max_normalValue:  # 1 of 16
					p.value = random.randint(max_normalValue,max_normalValue+16)
					if p.value > max_normalValue+15:  # 1 of 240 (1 of 15)
						p.value = random.randint(max_normalValue+15,max_normalValue+36)
						if p.value > max_normalValue+35: # 1 of 4800 (1 of 20)
							p.value = random.randint(max_normalValue+35,125)
					
				p.zone = 0
				p.other = None
				p.other2 = None
				p.edate = date
				p.sensor = sen
				p.save()

				if p.value > sen.alert:
					n = Notification()
					n.sensor = sen
					n.value = p.value
					n.ndate = p.edate
					n.viewed = True
					n.decription = "value exceed"
					n.save()
			date = date + timezone.timedelta(hours=1)

		date = now -timezone.timedelta(hours=1)
		while date < timezone.now(): #144
			for sen in sensors:
				p = Pollution()
				max_normalValue = int(float(sen.alert) * max_normalIndex)
		
				if max_normalValue < 15:
					p.value = random.randint(0,max_normalValue+2)
				else:
					p.value = random.randint(max_normalValue-15,max_normalValue+2)
					if p.value < max_normalValue-(15*(max_normalValue-15)/max_normalValue):
						p.value = random.randint(0,max_normalValue-(15*(max_normalValue-15)/max_normalValue))

				if p.value > max_normalValue:  # 1 of 16
					p.value = random.randint(max_normalValue,max_normalValue+16)
					if p.value > max_normalValue+15:  # 1 of 240 (1 of 15)
						p.value = random.randint(max_normalValue+15,max_normalValue+36)
						if p.value > max_normalValue+35: # 1 of 4800 (1 of 20)
							p.value = random.randint(max_normalValue+35,125)
					
				p.zone = 0
				p.other = None
				p.other2 = None
				p.edate = date
				p.sensor = sen
				p.save()

				if p.value > sen.alert:
					n = Notification()
					n.sensor = sen
					n.value = p.value
					n.ndate = p.edate
					n.viewed = False
					n.decription = "value exceed"
					n.save()
			date = date + timezone.timedelta(minutes=5)

		return render(request, self.template_name);