from django.db import models

class Sensor(models.Model):
	
	class Meta:
		db_table = "sensors"

	id_s = models.PositiveSmallIntegerField(primary_key=True)
	sensorType = models.PositiveSmallIntegerField(null=False, default=0)
	latitude = models.DecimalField(null=False, default=0, max_digits=12, decimal_places=10)
	longitude = models.DecimalField(null=False, default=0, max_digits=12, decimal_places=10)
	name = models.TextField(null=False, default="Unknown")
	description = models.TextField(null=True, default="Not description")
	alert = models.DecimalField(null=False, default=75, max_digits=4, decimal_places=1)

	def __unicode__(self):
		return unicode(self.id_s)
