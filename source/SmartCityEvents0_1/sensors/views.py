from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from .models import Sensor

class managementSensorView(TemplateView):
	template_name = 'sensors/managementSensor.html'
	sucess_url = reverse_lazy('management')

	def get(self, request):
		return render_to_response('sensors/managementSensor.html', context_instance=RequestContext(request));

class addSensorView(TemplateView):
	template_name = 'sensors/sensorAdd.html'
	model = Sensor
	sucess_url = reverse_lazy('add')

	def post(self, request, *args, **kwargs):
		
		sensor = Sensor()
		id_max = 1
		id_current = 1
		sensorsList = Sensor.objects.all()

		if len(sensorsList) == 0:
			sensor.id_s = 1
			id_current = id_current + 1
		else:
			for s in sensorsList:
				if(s.id_s>id_max):
					id_max = s.id_s

			while id_current < id_max:
				if len(Sensor.objects.filter(id_s=id_current)) == 0:
					sensor.id = id_current
					id_current = id_max + 1
				id_current = id_current + 1

		if id_current == id_max:
			sensor.id_s = id_max + 1

		sensor.sensorType = request.POST['type']
		if request.POST['name'] != "":
			sensor.name = request.POST['name']
		if request.POST['alert'] != "":
			sensor.alert = request.POST['alert']
		sensor.latitude = request.POST['lat']
		sensor.longitude = request.POST['lon']
		if request.POST['desc'] != "":
			sensor.description = request.POST['desc']
		sensor.save()
		state = True
		dic ={'state': state}
		return render_to_response('sensors/sensorAdd.html', dic, context_instance=RequestContext(request));

class removeSensorView(TemplateView):
	template_name = 'sensors/sensorRemove.html'
	model = Sensor
	sucess_url = reverse_lazy('remove')

	def post(self, request, *args, **kwargs):
		sensor = Sensor()
		sensor.id_s = request.POST['id']
		sensor.delete()
		state = True
		sensors = Sensor.objects.all()
		return render_to_response('sensors/sensorRemove.html', {'sensors': sensors, 'state': state}, context_instance=RequestContext(request));

	def get(self, request, *args, **kwargs):
		state= False
		sensors = Sensor.objects.all()
		return render_to_response('sensors/sensorRemove.html', {'sensors': sensors, 'state': state}, context_instance=RequestContext(request));

class modifySensorView(TemplateView):
	template_name = 'sensors/sensorModify.html'
	model = Sensor
	sucess_url = reverse_lazy('modify')

	def post(self, request, *args, **kwargs):
		sensor = Sensor()
		sensor.id_s = request.POST['id']

		newdata = request.POST.get('newtype', "")
		if newdata != "":
			sensor.sensorType = newdata
		else:
			sensor.sensorType = Sensor.objects.raw('SELECT * FROM sensors WHERE id_s='+sensor.id_s)[0].sensorType

		newdata = request.POST.get('newname', "")
		if newdata != "":
			sensor.name = newdata
		else:
			sensor.sensorType = Sensor.objects.raw('SELECT * FROM sensors WHERE id_s='+sensor.id_s)[0].sensorType

		newdata = request.POST.get('newlat', "")
		if newdata != "":
			sensor.latitude = newdata
		else:
			sensor.latitude = Sensor.objects.raw('SELECT * FROM sensors WHERE id_s='+sensor.id_s)[0].latitude

		newdata = request.POST.get('newlon', "")
		if newdata != "":
			sensor.longitude = newdata
		else:
			sensor.longitude = Sensor.objects.raw('SELECT * FROM sensors WHERE id_s='+sensor.id_s)[0].longitude

		newdata = request.POST.get('newdesc', "")
		if newdata != "":
			sensor.description = newdata
		else:
			sensor.description = str(Sensor.objects.raw('SELECT * FROM sensors WHERE id_s='+sensor.id_s)[0].description)

		sensor.save()
		state = True
		sensors = Sensor.objects.all()
		return render_to_response('sensors/sensorModify.html', {'sensors': sensors, 'state': state}, context_instance=RequestContext(request));

	def get(self, request, *args, **kwargs):
		state = False
		sensors = Sensor.objects.all()
		return render_to_response('sensors/sensorModify.html', {'sensors': sensors, 'state': state}, context_instance=RequestContext(request));

class mapSensorAddView(TemplateView):
	template_name = 'sensors/managementMapAdd.html'
	model = Sensor
	sucess_url = reverse_lazy('sensorsmapadd')

	def get(self, request):

		sensors = Sensor.objects.all()
		return render_to_response('sensors/managementMapAdd.html', {'sensors': sensors}, context_instance=RequestContext(request));

class mapSensorRemoveView(TemplateView):
	template_name = 'sensors/managementMapRemove.html'
	model = Sensor
	sucess_url = reverse_lazy('sensorsmapremove')

	def get(self, request):

		sensors = Sensor.objects.all()
		return render_to_response('sensors/managementMapRemove.html', {'sensors': sensors}, context_instance=RequestContext(request));

class mapSensorModifyView(TemplateView):
	template_name = 'sensors/managementMapModify.html'
	model = Sensor
	sucess_url = reverse_lazy('sensorsmapmodify')

	def get(self, request):

		sensors = Sensor.objects.all()
		return render_to_response('sensors/managementMapModify.html', {'sensors': sensors}, context_instance=RequestContext(request));

class mapLegendView(TemplateView):
	template_name = 'sensors/legendMap.html'
	model = Sensor
	sucess_url = reverse_lazy('legendMap')

	def get(self, request):

		return render_to_response('sensors/legendMap.html', context_instance=RequestContext(request));