# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id_s', models.PositiveSmallIntegerField(serialize=False, primary_key=True)),
                ('sensorType', models.PositiveSmallIntegerField(default=0)),
                ('latitude', models.DecimalField(default=0, max_digits=12, decimal_places=10)),
                ('longitude', models.DecimalField(default=0, max_digits=12, decimal_places=10)),
                ('name', models.TextField(default=b'Unknown')),
                ('description', models.TextField(default=b'Not description', null=True)),
                ('alert', models.DecimalField(default=75, max_digits=4, decimal_places=1)),
            ],
            options={
                'db_table': 'sensors',
            },
        ),
    ]
