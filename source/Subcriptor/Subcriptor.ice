// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

#pragma once

module Subcriptor 
{
	interface Events{
		idempotent void send(int valueEvent, int zoneEvent, int otherEvent, int other2Event, string dateEvent, int sensorEvent);
		
	};
};
