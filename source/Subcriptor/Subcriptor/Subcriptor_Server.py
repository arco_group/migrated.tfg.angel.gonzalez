#!/usr/bin/env python

import sys, Ice, os, rrdtool, MySQLdb
from datetime import datetime, date, time, timedelta
Ice.loadSlice('Subcriptor.ice')
Ice.updateModules()
import Subcriptor

#Sirviente:
class SubcriptorI(Subcriptor.Events):		

	def send(self, valueEvent, zoneEvent, otherEvent, other2Event, dateEvent, sensorEvent, current=None):
		
		print 'valor {} - zone {} - sensor {}'.format(valueEvent, zoneEvent, sensorEvent)

		dateEvent = datetime.strptime(dateEvent, "%Y-%m-%d %H:%M:%S") - timedelta(hours=1) 
		print '{}'.format(dateEvent)
	
		############## CODING EVENTS ###############

		#  1: NO2
		#  2: SO2
		#  3: CO2
		#  4: O3
		#  5: PM2.5
		#  6: PM10

		sql='''INSERT into pollution (value, zone, other, other2, edate, sensor_id) values ({0}, {1}, {2}, {3}, '{4}', {5})'''.format(valueEvent, 1, 0, 0, dateEvent, sensorEvent)


		try:
		########### USANDO RRDTOOL #################
		#	path = "../../DB/ejemplo.rrd"
		#	ret = rrdtool.update(path,'N:' + str(valueEvent))
    		#	if ret:
        	#		print rrdtool.error()
    		#	print u"Actualizando rrd_ejemplo con valor=%s" % valueEvent

		########### USANDO MYSQL #################
			db = MySQLdb.connect("localhost", "root", "root", "SmartCity_events")
			cursor = db.cursor()
			cursor.execute(sql)

			sql= "SELECT alert FROM sensors WHERE id_s = " + sensorEvent
			cursor.execute(sql)
			alertEvent = cursor.fetchone()[0]
			if value > alertEvent:
				sql='''INSERT into notifications (value, ndate, viewed, description, sensor_id) values ({0}, {1}, {2}, {3}, '{4}')'''.format(valueEvent, dateEvent, 0, "", sensorEvent)
				cursor.execute(sql)

			db.commit()
			db.close()
		except Exception, e:
   			print e

class Server(Ice.Application):
	def run(self, args):

		if len(args) > 1:
            		print(self.appName() + ": too many arguments")
			return 1

		#creacion de adaptador de objetos
		adapter = self.communicator().createObjectAdapter("Events")
		#Registro de sirviente en el adaptador
		adapter.add(SubcriptorI(), self.communicator().stringToIdentity("events"))
		#activacion de adaptador
		adapter.activate()
        	self.communicator().waitForShutdown()
		return 0

#Creacion de instacion del sirviente
sys.stdout.flush()
app = Server()
sys.exit(app.main(sys.argv, "config.server"))

